package com.bitbucket.ther0binio.compassglass;

import com.bitbucket.ther0binio.compassglass.util.DirectionGetter;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import com.bitbucket.ther0binio.compassglass.util.GetTheSpawn;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemGui {

    private Inventory inv;
    private ConfigurationSection tpItems;
    private Filemanager fm = new Filemanager();
    
    public ItemGui() {
        loadItemsInConfig();
        inv = Bukkit.createInventory(null, 27, "Men�");
        ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1);
        ItemMeta glassNoName = glass.getItemMeta();
        glassNoName.setDisplayName(" ");
        glass.setItemMeta(glassNoName);

        for (int i = 0; i < 27; i++) {
            inv.setItem(i, glass);
        }
        ItemStack spawn = new ItemStack(fm.getConfigInt("spawn.Item"), 1);
        ItemMeta spawnMeta = spawn.getItemMeta();
        spawnMeta.setDisplayName("Spawn");
        spawn.setItemMeta(spawnMeta);
        inv.setItem(13, spawn);
        tpItems = fm.getConfigSec("guiItems");
        for(String key : tpItems.getKeys(false)) {
           ItemStack guiItem = new ItemStack(fm.getConfigInt("guiItems."+key+".Item"), 1);
           ItemMeta nameTheItem = guiItem.getItemMeta();
           nameTheItem.setDisplayName(key);
           guiItem.setItemMeta(nameTheItem);
           inv.setItem(fm.getConfigInt("guiItems."+key+".Position"), guiItem);
        }
    }
    
    public void getGui(Player p) {
        p.openInventory(inv);
    }
    
    public boolean teleportItem(int item, Player p) {
        if (133 == item) {
             if(!(fm.getConfigInt("spawn.y") == 0)) {
                    GetTheSpawn getSpawn = new GetTheSpawn();
                    p.teleport(getSpawn.getSpawn());
            }
        }
        for(String key: tpItems.getKeys(false)) {
            if (fm.getConfigInt("guiItems."+key+".Item") == item) {
                if(!(fm.getConfigInt("guiItems."+key+".tppunkt.y") == 0)) {
                    
                    Location tpSpot = new Location(p.getWorld(), 0, 0, 0, 0, 0);
                    tpSpot.setX(fm.getConfigDouble("guiItems."+key+".tppunkt.x"));
                    tpSpot.setY(fm.getConfigDouble("guiItems."+key+".tppunkt.y"));
                    tpSpot.setZ(fm.getConfigDouble("guiItems."+key+".tppunkt.z"));
                    tpSpot.setYaw(DirectionGetter.getDirection(fm.getConfigString("guiItems."+key+".tppunkt.direction")));
                    p.teleport(tpSpot);
                    return true;
                }
            }
        }
        return false;
    }
    
    public Inventory loadItemsInConfig() {
        return inv;
    }
}

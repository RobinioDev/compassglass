package com.bitbucket.ther0binio.compassglass;

import com.bitbucket.ther0binio.compassglass.commands.CMD_Addwrap;
import com.bitbucket.ther0binio.compassglass.commands.CMD_Edit;
import com.bitbucket.ther0binio.compassglass.commands.CMD_Build;
import com.bitbucket.ther0binio.compassglass.commands.CMD_Fly;
import com.bitbucket.ther0binio.compassglass.commands.CMD_Reload;
import com.bitbucket.ther0binio.compassglass.commands.CMD_SetSpawn;
import com.bitbucket.ther0binio.compassglass.commands.CMD_Spawn;
import com.bitbucket.ther0binio.compassglass.commands.SubCommand;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHandler implements CommandExecutor {

    private HashMap<String, SubCommand> commands;

    ArrayList<Player> players = new ArrayList<>();
    private MessageHelper mHelper = new MessageHelper();

    public CommandHandler() {
        commands = new HashMap<String, SubCommand>();
        loadCommands();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Lobby Commands sind nur f�r Spieler");
            return true;
            }
            Player p = (Player) sender;
            if (command.getName().equalsIgnoreCase("spawn")) {
                CMD_Spawn spawn = new CMD_Spawn() {};
                spawn.onCommand(p, args);
                return true;
            }
            if (command.getName().equalsIgnoreCase("friend")) {
                if (args.length != 0) {
                    if (args[0].equalsIgnoreCase("add")) {
                        commands.get("friend add").onCommand(p, args);
                        return true;
                    }
                }
                mHelper.sendMessage(p, "Benutze bitte das GUI. Adden kannst du Spieler mit /friend add");
                return true;
            }
            if (command.getName().equalsIgnoreCase("lobby")) {

                if (args.length == 0) {
                    mHelper.sendMessage(p, ChatColor.GOLD + "Das LobbyPlugin wurde von TheRobinio entwickelt.");
                    return true;
                }

                if (args[0].equalsIgnoreCase("help")) {
                    help(p);
                    return true;
                }
                String sub = args[0];
                ArrayList<String> c = new ArrayList<>();
                c.addAll(Arrays.asList(args));
                c.remove(0);
                args = (String[]) c.toArray(new String[0]);
                if (!(commands.containsKey(sub))) {
                    mHelper.sendMessage(p, "&cDiesen Command gibt es nicht!");
                    mHelper.sendMessage(p, "&cNutze /lobby help um alle Commands zu sehen");
                    return true;
                }
                try {
                    commands.get(sub).onCommand(p, args);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHelper.sendMessage(p, "&cFehler mit dem Command Executer");
                    mHelper.sendMessage(p, "&cNutze /lobby help f�r mehr Informationen");
                }
                
                return true;
            }
            return false;
        
    }

    private void loadCommands() {
           commands.put("setspawn", new CMD_SetSpawn());
           commands.put("spawn", new CMD_Spawn());
           commands.put("build", new CMD_Build());
           commands.put("reload", new CMD_Reload());
           commands.put("fly", new CMD_Fly());
           commands.put("edit", new CMD_Edit());
           commands.put("addwrap", new CMD_Addwrap());
    }

    private void help(Player p) {
        mHelper.sendMessage(p, "&6---HELP---");
        for (String command : commands.keySet()) {
            try {
                String commandStri = commands.get(command).getCommand(p);
                String commandHelpText = commands.get(command).help(p);
                if (!(commandStri == null)) {
                mHelper.sendMessage(p, ChatColor.GREEN + commandStri+ " " + ChatColor.WHITE + commandHelpText);
                }
            } catch (Exception e) {
                
            }
        }

    }

}

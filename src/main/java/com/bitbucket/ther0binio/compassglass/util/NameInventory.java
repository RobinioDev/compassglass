package com.bitbucket.ther0binio.compassglass.util;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import static org.bukkit.event.inventory.InventoryType.ANVIL;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NameInventory {

    Inventory nameInv;
    
    public void getNameInv(Player p) {
        nameInv = Bukkit.createInventory(p, ANVIL, "NewWrap");
        ItemStack confirmWool = new ItemStack(Material.WOOL, 1, (byte) 5);
        ItemMeta confirmWoolMeta = confirmWool.getItemMeta();
        confirmWoolMeta.setDisplayName("Speichern");
        confirmWool.setItemMeta(confirmWoolMeta);
        nameInv.setItem(2, confirmWool);
        p.openInventory(nameInv);
    }
}

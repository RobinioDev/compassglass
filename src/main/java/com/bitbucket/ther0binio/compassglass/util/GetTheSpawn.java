package com.bitbucket.ther0binio.compassglass.util;

import static org.bukkit.Bukkit.getServer;
import org.bukkit.Location;
import org.bukkit.World;

public class GetTheSpawn {

    private Filemanager fm = new Filemanager();
    
    public Location getSpawn() {
        World w = getServer().getWorld(fm.getConfigString("spawn.world"));
        Location loc = new Location(w, 0, 0, 0, 0, 0);
        loc.setX(fm.getConfigInt("spawn.x"));
        loc.setY(fm.getConfigInt("spawn.y"));
        loc.setZ(fm.getConfigInt("spawn.z"));
        loc.setYaw(DirectionGetter.getDirection(fm.getConfigString("spawn.direction")));
        return loc;
    }
}

package com.bitbucket.ther0binio.compassglass.util;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

public class Filemanager {
    
    public static Plugin plugin;
    
    //FD = FileData
    public String getConfigString(String path) {
        return plugin.getConfig().getString(path);
    }
    public int getConfigInt(String path) {
        return plugin.getConfig().getInt(path);
    }
    public Double getConfigDouble(String path) {
        return plugin.getConfig().getDouble(path);
    }
    
    public ConfigurationSection getConfigSec(String path) {
        return plugin.getConfig().getConfigurationSection(path);
    }
    
    public void setConfigFD(String path, String s) {
        plugin.getConfig().set(path, s);
        plugin.saveConfig();
        plugin.reloadConfig();
    }
    
    public void setConfigFD(String path, int i) {
        plugin.getConfig().set(path, i);
        plugin.saveConfig();
        plugin.reloadConfig();
    }
    
    public void setConfigFD(String path, Double d) {
        plugin.getConfig().set(path, d);
        plugin.saveConfig();
        plugin.reloadConfig();
    }
    public void reloadConfigFD() {
        plugin.reloadConfig();
    }
}

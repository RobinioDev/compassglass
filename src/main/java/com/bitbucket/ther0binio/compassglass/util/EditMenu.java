package com.bitbucket.ther0binio.compassglass.util;

import com.bitbucket.ther0binio.compassglass.ItemGui;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class EditMenu {

    Inventory newInv;
    
    public EditMenu() {
        newInv = Bukkit.createInventory(null, 54, "Edit Men�");
        ItemStack editItem = new ItemStack(Material.REDSTONE_COMPARATOR, 1);
        ItemMeta editItemMeta = editItem.getItemMeta();
        editItemMeta.setDisplayName("Move Men�Item");
        editItem.setItemMeta(editItemMeta);
        newInv.setItem(38, editItem);
    }
    
    public void getTheEditor(Player p) {
        ItemGui oldGui = new ItemGui();
        Inventory switcher = oldGui.loadItemsInConfig();
        for (int i = 0; i < 27; i++) {
            newInv.setItem(i, switcher.getItem(i));
        }
        p.openInventory(newInv);
    }
    
    public void editMenu(String key, Player p) {
        Inventory configurateInv = Bukkit.createInventory(null, 54, "Configuration");
        ItemStack confirmWool = new ItemStack(Material.WOOL, 1, (byte) 14);
        ItemMeta confirmWoolMeta = confirmWool.getItemMeta();
        confirmWoolMeta.setDisplayName("L�schen");
        confirmWool.setItemMeta(confirmWoolMeta);
        configurateInv.setItem(38, confirmWool);
        p.openInventory(configurateInv);
    }
}

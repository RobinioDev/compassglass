package com.bitbucket.ther0binio.compassglass.util;

public class DirectionGetter {
 
    private static String[] directions = new String[]{"north", "south", "east", "west"};
    
    public static float getDirection(String direction) {
        
                    

            if (directions[0].equalsIgnoreCase(direction)) {
                return 180;
            }
            if (directions[1].equalsIgnoreCase(direction)) {
                return 0;
            }
            if (directions[2].equalsIgnoreCase(direction)) {
                return 270;
            }
            if (directions[3].equalsIgnoreCase(direction)) {
                return 90;
            }
            return 0;
    }
    
    public static String setDirection(float f) {
        float coord1 = 315;
        float coord2 = 45;
        float coord3 = 135;
        float coord4 = 225;
        if (f > coord1|| f < coord2) {
            return directions[1]; 
        }
        if (f > coord2 && f < coord3) {
            return directions[3];
        }
        if (f > coord3 && f < coord4) {
            return directions[0];
        }
        if (f > coord4 && f < coord1) {
            return directions[2];
        }
        return null;
    }
}

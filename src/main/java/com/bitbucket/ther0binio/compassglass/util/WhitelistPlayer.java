package com.bitbucket.ther0binio.compassglass.util;

import java.util.ArrayList;
import org.bukkit.entity.Player;

public class WhitelistPlayer {
    
    //Allow Players to build in the Lobby
    public static ArrayList<Player> allowedBuilders = new ArrayList<Player>();
    //Allow Player to fly
    public static ArrayList<Player> flyingPlayers = new ArrayList<Player>();
    
    public static void addPlayerToBuilderList(Player p) {
        allowedBuilders.add(p); 
    }
    public static void addPlayerToFlyingList(Player p) {
        flyingPlayers.add(p);
    }
}

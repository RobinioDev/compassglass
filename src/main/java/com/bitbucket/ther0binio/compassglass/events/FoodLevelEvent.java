package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelEvent implements Listener{

    private Filemanager fm = new Filemanager();
    
    @EventHandler
    public boolean onItemPickUp(FoodLevelChangeEvent e) {
        Player p = (Player) e.getEntity();
        if (!(p.getLocation().getWorld().getName().equalsIgnoreCase(fm.getConfigString("spawn.world")))) {
            return true;
        }
        if (p.hasPermission("compassglass.user")) {
            e.setCancelled(true);
        }
        return true;
    }
}

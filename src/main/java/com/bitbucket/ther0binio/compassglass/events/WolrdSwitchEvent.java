package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import com.bitbucket.ther0binio.compassglass.util.WhitelistPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

public class WolrdSwitchEvent implements Listener{

    private Filemanager fm = new Filemanager();

    
    @EventHandler
    public boolean onChangeWorld(PlayerChangedWorldEvent e) {
        Player p = (Player) e.getPlayer();
        if (!(e.getFrom().getName().equalsIgnoreCase(fm.getConfigString("spawn.world")))) {
            return true;
        }
        for (int i = 0; i < WhitelistPlayer.allowedBuilders.size(); i++) {
            if(WhitelistPlayer.allowedBuilders.get(i) == p) {
                WhitelistPlayer.allowedBuilders.remove(i);
                return true;
            }
        }
        for (int i = 0; i < WhitelistPlayer.flyingPlayers.size(); i++) {
            if(WhitelistPlayer.flyingPlayers.get(i) == p) {
                p.setAllowFlight(false);
                WhitelistPlayer.flyingPlayers.remove(i);
                return true;
            }
        }
        return true;
    }
}

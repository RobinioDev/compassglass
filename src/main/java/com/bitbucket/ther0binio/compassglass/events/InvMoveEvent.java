package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.ItemGui;
import com.bitbucket.ther0binio.compassglass.util.DirectionGetter;
import com.bitbucket.ther0binio.compassglass.util.EditMenu;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InvMoveEvent implements Listener{

    private ItemGui guiGetter;
    private ConfigurationSection tpItems;
    private Filemanager fm = new Filemanager();
    private EditMenu eM = new EditMenu();
    private int currentlyEditing;
    
    public InvMoveEvent () {
        guiGetter = new ItemGui();
    }
    
    @EventHandler
    public boolean onItemMove(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        tpItems = fm.getConfigSec("guiItems");
        
        int item = 0;
        Material itemMa = null;
        try {
            item = e.getCurrentItem().getTypeId();
            itemMa = e.getCurrentItem().getType();
        } catch (Exception ex) {}
        if (e.getInventory().getName().equalsIgnoreCase("Men�")) {
            guiGetter.teleportItem(item, p);
            e.setCancelled(true);
            return true;
        }
        if (!(p.hasPermission("compassglass.admin"))) {
            e.setCancelled(true);
            return true;
        }
        if (e.getInventory().getName().equalsIgnoreCase("Edit Men�")) {
            if (itemMa == Material.REDSTONE_COMPARATOR) {
                
            }
            for(String key: tpItems.getKeys(false)) {
                if (fm.getConfigInt("guiItems."+key+".Item") == item) {
                    currentlyEditing = item;
                    eM.editMenu(key, p);
                }
            }
            e.setCancelled(true);
            return true;
        }
        if  (e.getInventory().getName().equalsIgnoreCase("Configuration")) {
            if (itemMa == Material.WOOL) {
                for(String key: tpItems.getKeys(false)) {
                    if (fm.getConfigInt("guiItems."+key+".Item") == currentlyEditing) {
                        fm.setConfigFD("guiItems."+key, (String) null);
                        p.closeInventory();
                }
              }
            }
            e.setCancelled(true);
            return true;
        }
        return true;
    }
    
}

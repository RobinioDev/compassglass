package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import com.bitbucket.ther0binio.compassglass.util.WhitelistPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class NoDestroyEvent implements Listener{

    private Filemanager fm = new Filemanager();
    
    @EventHandler
    public boolean onDestroy(BlockBreakEvent e) {
        Player p = (Player) e.getPlayer();
        if (!(p.getLocation().getWorld().getName().equalsIgnoreCase(fm.getConfigString("spawn.world")))) {
            return true;
        }
        for (int i = 0; i < WhitelistPlayer.allowedBuilders.size(); i++) {
            if(WhitelistPlayer.allowedBuilders.get(i) == p) {
                return true;
            }
        }
        e.setCancelled(true);
        return true;
    }
}

package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.ItemHandler;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinEvent implements Listener {

    private ItemHandler itemGetter = new ItemHandler();
    private Filemanager fm = new Filemanager();
    
    @EventHandler
    public boolean onJoin(PlayerJoinEvent e) {
        Player p = (Player) e.getPlayer();
        if (p.hasPermission("compassglass.join")) {
            e.setJoinMessage("&b"+ p.getName() + "&e ist dem Spiel beigetreten.");
        }
        e.setJoinMessage(null);
        p.sendTitle(ChatColor.getByChar(fm.getConfigString("titleColor")) + fm.getConfigString("titleText"), null);
        itemGetter.setItem(p);
        return true;
    }
}

package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageEvent implements Listener {

    private Filemanager fm = new Filemanager();
    
    @EventHandler
    public boolean onDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) {
            return true;
        }
        Player p = (Player) e.getEntity();
        if (!(p.getLocation().getWorld().getName().equalsIgnoreCase(fm.getConfigString("spawn.world")))) {
            return true;
        }
        if (p.hasPermission("compassglass.user")) {
            e.setCancelled(true);
        }
        return true;
    }
}

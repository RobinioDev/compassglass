package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.ItemGui;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class RightClickEvent implements Listener{

    private Filemanager fm = new Filemanager();
    
    @EventHandler
    public boolean onClick(PlayerInteractEvent e) {
        Player p = (Player) e.getPlayer();
        
        if (!(p.getLocation().getWorld().getName().equalsIgnoreCase(fm.getConfigString("spawn.world")))) {
            return true;
        }
        ItemStack item = e.getItem();
        if (item == null) {return true;}
        if(item.getTypeId() == fm.getConfigInt("item")) {
            ItemGui guiGetter = new ItemGui();
            guiGetter.getGui(p);
            return true;
        }
        return true;
    }
}

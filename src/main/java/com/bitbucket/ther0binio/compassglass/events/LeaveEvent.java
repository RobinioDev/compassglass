package com.bitbucket.ther0binio.compassglass.events;

import com.bitbucket.ther0binio.compassglass.util.WhitelistPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveEvent implements Listener{

    @EventHandler
    public boolean onLeave(PlayerQuitEvent e) {
        Player p = (Player) e.getPlayer();
        for (int i = 0; i < WhitelistPlayer.allowedBuilders.size(); i++) {
            if(WhitelistPlayer.allowedBuilders.get(i) == p) {
                WhitelistPlayer.allowedBuilders.remove(i);
                return true;
            }
        }
        for (int i = 0; i < WhitelistPlayer.flyingPlayers.size(); i++) {
            if(WhitelistPlayer.flyingPlayers.get(i) == p) {
                p.setAllowFlight(false);
                WhitelistPlayer.flyingPlayers.remove(i);
                return true;
            }
        }
        e.setQuitMessage(null);
        return true;
    }
}

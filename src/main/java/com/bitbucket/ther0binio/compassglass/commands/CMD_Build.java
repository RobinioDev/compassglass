package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.WhitelistPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CMD_Build implements SubCommand {

    private MessageHelper mHelper = new MessageHelper();
    private Player allowedP;
    
    @Override
    public boolean onCommand(Player player, String[] args) {
        if(!(player.hasPermission(permission()))){
            mHelper.sendMessage(player, "&c Du hast keine Berechtigungen dazu!");
            return true;
        }
        if (!(args.length == 0)) {
            Player target = (Bukkit.getServer().getPlayer(args[0]));
            if (!(target == null)) {
                allowedP = target;
            }
        } else {
            allowedP = player;
        }
        for (int i = 0; i < WhitelistPlayer.allowedBuilders.size(); i++) {
            if(WhitelistPlayer.allowedBuilders.get(i) == allowedP) {
                mHelper.sendMessage(allowedP, "&a Du kannst nun nicht mehr bauen");
                WhitelistPlayer.allowedBuilders.remove(i);
                return true;             
            }
        }
        WhitelistPlayer.addPlayerToBuilderList(allowedP);
        mHelper.sendMessage(allowedP, "&a Du kannst nun in der Lobby bauen");
        return true;
    }

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "Erlaubt es dir zu Bauen in der Lobby oder nicht mehr zu bauen";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/lobby build (Player)";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.build";
    }

}

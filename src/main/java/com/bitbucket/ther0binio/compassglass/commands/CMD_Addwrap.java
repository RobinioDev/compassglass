package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.NameInventory;
import org.bukkit.entity.Player;

public class CMD_Addwrap implements SubCommand{

    private MessageHelper mHelper = new MessageHelper();
    
    @Override
    public boolean onCommand(Player player, String[] args) {
        if(!(player.hasPermission(permission()))){
            mHelper.sendMessage(player, "&c Du hast keine Berechtigungen dazu!");
            return true;
        }
        if(!(args.length == 1)) {
            mHelper.sendMessage(player, "&1 /lobby addwrap <Name>");
        }
        NameInventory anv = new NameInventory();
        anv.getNameInv(player);
        return true;
    }

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "Erstelle einen Wrap Punkt im Men�";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/lobby addwrap <Name>";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.admin";
    }

}
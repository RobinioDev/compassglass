/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.GetTheSpawn;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author noahs
 */
public class CMD_Spawn implements SubCommand{
    
    private MessageHelper mHelper = new MessageHelper();

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "Sendet dich zum Spawn";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/spawn";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.spawn";
    }

    @Override
    public boolean onCommand(Player player, String[] args) {
        if(player.hasPermission(permission())){
            GetTheSpawn getSpawn = new GetTheSpawn();
            player.teleport(getSpawn.getSpawn());
            mHelper.sendMessage(player, ChatColor.GREEN + "Du bist jetzt wieder am Spawn");
            return true;
        }
        mHelper.noPerm(player);
        return true;
        
    }
    
    
}

package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.DirectionGetter;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class CMD_SetSpawn implements SubCommand {

    private MessageHelper mHelper = new MessageHelper();
    private Filemanager fm = new Filemanager();
    
    @Override
    public boolean onCommand(Player player, String[] args) {
        if(!(player.hasPermission(permission()))){
            mHelper.sendMessage(player, "&c Du hast keine Berechtigungen dazu!");
            return true;
        }
        Location loc = player.getLocation();
        fm.setConfigFD("spawn.world", loc.getWorld().getName());
        fm.setConfigFD("spawn.x", loc.getX());
        fm.setConfigFD("spawn.y", loc.getY());
        fm.setConfigFD("spawn.z", loc.getZ());
        fm.setConfigFD("spawn.direction", DirectionGetter.setDirection(loc.getYaw()));
        mHelper.sendMessage(player, "&4 Der Spawn wurde erfolgreich gesetzt!");
        return true;
    }

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "Sendet dich zum Spawn";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/lobby setspawn";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.admin";
    }
}

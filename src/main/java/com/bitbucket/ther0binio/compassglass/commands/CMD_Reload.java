package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import org.bukkit.entity.Player;

public class CMD_Reload implements SubCommand{

    private MessageHelper mHelper = new MessageHelper();
    private Filemanager fm = new Filemanager();
    
    @Override
    public boolean onCommand(Player player, String[] args) {
        if(!(player.hasPermission(permission()))){
            mHelper.sendMessage(player, "&c Du hast keine Berechtigungen dazu!");
            return true;
        }
        fm.reloadConfigFD();
        mHelper.sendMessage(player, "&4 Die Config wurde neugeladen");
        return true;
    }

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "L�dt die config.yml nachdem du sie ver�ndert hast.";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/lobby reload";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.admin";
    }

}

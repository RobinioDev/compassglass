package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.EditMenu;
import org.bukkit.entity.Player;

public class CMD_Edit implements SubCommand{

    private MessageHelper mHelper = new MessageHelper();

    @Override
    public boolean onCommand(Player player, String[] args) {
        if(!(player.hasPermission(permission()))){
            mHelper.sendMessage(player, "&c Du hast keine Berechtigungen dazu!");
            return true;
        }
        EditMenu createInv = new EditMenu();
        createInv.getTheEditor(player);
        return true;
    }

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "Edetiere das Lobbylayout";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/lobby edit";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.admin";
    }

}

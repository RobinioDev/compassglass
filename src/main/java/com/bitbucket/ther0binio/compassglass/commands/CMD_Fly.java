package com.bitbucket.ther0binio.compassglass.commands;

import com.bitbucket.ther0binio.compassglass.MessageHelper;
import com.bitbucket.ther0binio.compassglass.util.WhitelistPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class CMD_Fly implements SubCommand{

    private MessageHelper mHelper = new MessageHelper();
    private Player allowedP;
    
    @Override
    public boolean onCommand(Player player, String[] args) {
        if(!(player.hasPermission(permission()))){
            mHelper.sendMessage(player, "&c Du hast keine Berechtigungen dazu!");
            return true;
        }
        if (!(args.length == 0)) {
            Player target = (Bukkit.getServer().getPlayer(args[0]));
            if (!(target == null)) {
                allowedP = target;
            }
        } else {
            allowedP = player;
        }
        for (int i = 0; i < WhitelistPlayer.flyingPlayers.size(); i++) {
            if(WhitelistPlayer.flyingPlayers.get(i) == allowedP) {
                allowedP.setAllowFlight(false);
                mHelper.sendMessage(allowedP, "&a Du kannst nun nicht mehr fliegen");
                WhitelistPlayer.flyingPlayers.remove(i);
                return true;             
            }
        }
        WhitelistPlayer.addPlayerToFlyingList(allowedP);
        mHelper.sendMessage(allowedP, "&a Du kannst nun in der Lobby fliegen");
        allowedP.setAllowFlight(true);
        return true;
    }

    @Override
    public String help(Player p) {
        if(p.hasPermission(permission())){
            return "Fliege durch die Welt";
        }
        return null;
    }

    @Override
    public String getCommand(Player p) {
        if(p.hasPermission(permission())){
            return "/lobby fly";
        }
        return null;
    }

    @Override
    public String permission() {
        return "compassglass.fly";
    }

}

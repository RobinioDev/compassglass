/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitbucket.ther0binio.compassglass.commands;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author noahs
 */
public interface SubCommand {
    
    public boolean onCommand(Player player, String [] args);
    
    public String help(Player p);
    
    public String getCommand(Player p);
    
    public String permission();
    
}

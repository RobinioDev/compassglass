package com.bitbucket.ther0binio.compassglass;

import com.bitbucket.ther0binio.compassglass.events.DamageEvent;
import com.bitbucket.ther0binio.compassglass.events.FoodLevelEvent;
import com.bitbucket.ther0binio.compassglass.events.PickUpItemEvent;
import com.bitbucket.ther0binio.compassglass.events.JoinEvent;
import com.bitbucket.ther0binio.compassglass.events.InvMoveEvent;
import com.bitbucket.ther0binio.compassglass.events.LeaveEvent;
import com.bitbucket.ther0binio.compassglass.events.NoDestroyEvent;
import com.bitbucket.ther0binio.compassglass.events.PlaceEvent;
import com.bitbucket.ther0binio.compassglass.events.RightClickEvent;
import com.bitbucket.ther0binio.compassglass.events.WolrdSwitchEvent;
import com.bitbucket.ther0binio.compassglass.util.Filemanager;
import java.util.logging.Logger;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class CompassGlass extends JavaPlugin {

    private final CompassGlass g = this;
    private ItemHandler itemGetter;
    private Logger logger;
    private PluginManager pm;
    

    
    @Override
    public void onEnable() {
        pm = getServer().getPluginManager();
        logger = g.getLogger();
        if(checkForPlugin()) {
        loadConfig();
        MessageHelper.plugin = g;
        Filemanager.plugin = g;
        g.getCommand("spawn").setExecutor(new CommandHandler());
        g.getCommand("lobby").setExecutor(new CommandHandler());
        g.getCommand("friend").setExecutor(new CommandHandler());
        pm.registerEvents(new PlaceEvent(), g);
        pm.registerEvents(new PickUpItemEvent(), g);
        pm.registerEvents(new JoinEvent(), g);
        pm.registerEvents(new InvMoveEvent(), g);
        pm.registerEvents(new RightClickEvent(), g);
        pm.registerEvents(new NoDestroyEvent(), g);
        pm.registerEvents(new FoodLevelEvent(), g);
        pm.registerEvents(new DamageEvent(), g);
        pm.registerEvents(new WolrdSwitchEvent(), g);
        pm.registerEvents(new LeaveEvent(), g);
        }
    }
    
    @Override
    public void onDisable() {
        
    }
   
    public boolean checkForPlugin() {
        /*if(!(pm.getPlugin("TitleAPI").isEnabled())) {
            logger.warning("Please install the TitleAPI");
            pm.disablePlugin(g);
            return false;
        }*/
        return true;
    }
    
    public void loadConfig() {
        try {
        saveResource("config.yml", false); } catch(Exception e) {}
        saveConfig();
    }
    

}

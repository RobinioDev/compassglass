/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitbucket.ther0binio.compassglass;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author noahs
 */
public class MessageHelper {
    
    public static Plugin plugin;
    private String prefix;
    
    public MessageHelper() {
        prefix = ChatColor.GRAY + "[" + ChatColor.DARK_GREEN + plugin.getConfig().getString("servername") + ChatColor.GRAY + "]" + ChatColor.RESET;
    }
    
    public void sendMessage(Player p, String message){
       String newmessage;
       newmessage = ChatColor.translateAlternateColorCodes('&', message);
       p.sendMessage(prefix + " " + newmessage);
        
        
    }
    public void sendMessage(CommandSender p, String message){
       String newmessage;
       newmessage = ChatColor.translateAlternateColorCodes('&', message);        
       p.sendMessage(prefix + " " + newmessage);
        
        
    }
    
    public void noPerm(Player p ){
        
        p.sendMessage(prefix + " �cDir fehlt die Berechtigung dazu!");
    }
    
    
    
}
